/**
 * 7 x 6
 * 
 *@author Ewen Belbeoch
 */

public class Jeu {
    
    /**
     * the score of the game
     */
    private int score;

    /**
     * the game board
     */
    private String[][] plateau = new String[6][7]; // 6 lignes et 7 colonnes

    /**
     * the number of the turn
     */
    private int tour = 0;

    /**
     * Create a new Jeu object.
     */
    public Jeu() {

        setScore(0);

        setPlateau();

        setTour();

    }

    /**
     * set the score of the game
     * 
     * @param score the score of the game
     */
    private void setScore(int score) {

        this.score = score;

    }

    /**
     * return the score of the game
     * 
     * @return the score of the game
     */
    public int getScore() {

        return score;

    }

    /**
     * set the game board
     */
    private void setPlateau() {

        for (int i = 0; i < plateau.length; i++) {

            for (int j = 0; j < plateau[i].length; j++) {

                plateau[i][j] = "_";

            }

        }

    }

    /**
     * print the game board
     */
    public void ToString() {

        for (int i = 0; i < plateau.length; i++) {

            for (int j = 0; j < plateau[i].length; j++) {

                System.out.print("| " + plateau[i][j] + " |");

            }

            System.out.println("\n-----------------------------------");

        }

    }

    /**
     * set the number of the turn
     */
    private void setTour() {

        this.tour = 0;

    }

    /**
     * return the number of the turn
     * 
     * @return the number of the turn
     */
    public int getTour() {

        return tour;

    }
    
    /**
     * set the value of the game board
     * 
     * @param ligne the line of the game board
     * @param colonne the column of the game board
     * @param valeur the value of the game board
     */
    public void setPlateau(int ligne, int colonne, String valeur) {
        
        plateau[ligne][colonne - 1] = valeur;

        tour++;
        
    }

    /**
     * return the value of the game board
     * 
     * @param colonne the column of the game board
     * @return the value of the game board
     */
    public int colonneVide(int colonne) {

        int ret = -1;

        if (colonne >= 1 && colonne <= 7) {

            for (int i = 0; i < plateau.length; i++) {

                if (plateau[i][colonne - 1] == "_") {
    
                    ret = i;
    
                }
    
            }

        }

        return ret;

    }

    /**
     * check if the game is over and who win and set the score
     * 
     * @param jeu the game
     */
    public void win(Jeu jeu) {

        if (jeu.getTour() == 42) {

            jeu.setScore(3);

        } else {

            for (int i = plateau.length - 1; i > plateau.length - 7; i--) {

                for (int j = 0; j < plateau[i].length; j++) {

                    // all the possibilities to win for the player 1 "x"
                    if (plateau[i][j] == "x") {

                        if (j < 4 && plateau[i][j + 1] == "x" && plateau[i][j + 2] == "x" && plateau[i][j + 3] == "x") {

                            setScore(1);

                        }

                        if (i > 2 && plateau[i - 1][j] == "x" && plateau[i - 2][j] == "x" && plateau[i - 3][j] == "x") {

                            setScore(1);

                        }

                        if (j < 4 && i > 2 && plateau[i - 1][j + 1] == "x" && plateau[i - 2][j + 2] == "x" && plateau[i - 3][j + 3] == "x") {

                            setScore(1);

                        }

                        if (j > 2 && i > 2 && plateau[i - 1][j - 1] == "x" && plateau[i - 2][j - 2] == "x" && plateau[i - 3][j - 3] == "x") {

                            setScore(1);

                        }

                    }

                    // all the possibilities to win for the player 2 "o"
                    if (plateau[i][j] == "o") {

                        if (j < 4 && plateau[i][j + 1] == "o" && plateau[i][j + 2] == "o" && plateau[i][j + 3] == "o") {

                            setScore(2);

                        }

                        if (i > 2 && plateau[i - 1][j] == "o" && plateau[i - 2][j] == "o" && plateau[i - 3][j] == "o") {

                            setScore(2);

                        }

                        if (j < 4 && i > 2 && plateau[i - 1][j + 1] == "o" && plateau[i - 2][j + 2] == "o" && plateau[i - 3][j + 3] == "o") {

                            setScore(2);

                        }

                        if (j > 2 && i > 2 && plateau[i - 1][j - 1] == "o" && plateau[i - 2][j - 2] == "o" && plateau[i - 3][j - 3] == "o") {

                            setScore(2);

                        }

                    }

                }

            }

        }

    }

}
