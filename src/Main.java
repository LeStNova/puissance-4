/**
 * 
 * @author Ewen Belbeoch
 */

class Main {

    /**
     * launched the game
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Jeu jeu = new Jeu();

        jeu.ToString();

        int test = whoPlayed(jeu);

        while (jeu.getScore() == 0) {

            play(jeu, test);

            jeu.win(jeu);

            test = whoPlayed(jeu);

        }

        if (jeu.getScore() == 1) {

            System.out.println("Joueur 1 a gagné !");

        } else if (jeu.getScore() == 2) {

            System.out.println("Joueur 2 a gagné !");

        } else if (jeu.getScore() == 3) {

            System.out.println("Match nul !");

        }
        
    }

    /**
     * return the player who played this turn (0 for the player 1 
     * or 1 for the player 2)
     * 
     * @param jeu the game
     * @return the player who played
     */
    public static int whoPlayed(Jeu jeu) {

        int tour = jeu.getTour();

        int ret;

        if (tour % 2 == 0) {

            System.out.println("Joueur 1");

            ret = 0;

        } else {

            System.out.println("Joueur 2");

            ret = 1;

        }

        return ret;

    }

    /**
     * play a turn and show the game after a turn
     * 
     * @param jeu the game
     * @param player the player who played
     */
    public static void play(Jeu jeu, int player) {

        int colonne = SimpleInput.getInt("Choisissez une colonne : ");

        int ligne = jeu.colonneVide(colonne);

        while (colonne < 1 || colonne > 7 || ligne == -1) {

            colonne = SimpleInput.getInt("Choisissez une colonne : ");

            ligne = jeu.colonneVide(colonne);

        }
        
        if (player == 0) {

            jeu.setPlateau(ligne, colonne, "x");

        } else {

            jeu.setPlateau(ligne, colonne, "o");

        }

        jeu.ToString();

    }

}