# Puissance 4 Java


## Here is the power 4 game I developed in Java 

if you need to compile my code, you just need to be in the "ws" directory 
and execute the commands:

```
javac -cp ../class -d ../class ../src/Main.java
javac -cp ../class -d ../class ../src/Jeu.java
javac -cp ../class -d ../class ../src/SimpleInput.java
```

and to start playing you just need to run the command 

```
java -cp ../class Main
```

which will launch program and the game of the shot.


# Enjoy the game ^^
